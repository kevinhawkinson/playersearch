import React from 'react';
import Modal from 'react-modal';
import PropTypes from 'prop-types';

const styles = {
  content : {
    left: '20%',
    right: '20%',
    bottom: '30%'
  }
};

export const PlayerModal = props => {
  const { player, onModalClose } = props;
  return (
    <Modal appElement={document.getElementById('root')} isOpen={true} contentLabel="Player" style={styles}>
      <div className="container-fluid text-center">    
        <div className="row content">
          <div className="col-sm-2 sidenav">
            <img src={player.photo}/>
          </div>
          <div className="col-sm-8 text-center"> 
            <h1>{player.firstName} {player.lastName}</h1>
          </div>
          <div className="col-sm-2 sidenav">
            <div className="well">
              <p><strong>Team:</strong> {player.team}</p>
            </div>
            <div className="well">
              <p><strong>Position:</strong> {player.position}</p>
            </div>
          </div>
          <footer className="container-fluid text-center">
            <button onClick={onModalClose}>close</button>
          </footer>
        </div>
      </div>
    </Modal>
  );
}

PlayerModal.propTypes = {
  player: PropTypes.shape({
    photo: PropTypes.string,
    firstName: PropTypes.string,
    lastName: PropTypes.string,
    team: PropTypes.string,
    position: PropTypes.string
  }),
  onModalClose: PropTypes.func
}
