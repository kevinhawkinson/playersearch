import React from 'react';
import PropTypes from 'prop-types';

export const Player = (props) => {
  const handleOnClick = () => {
    const { onClick, player } = props;
    onClick(player);
  };

  const { player: { team, lastName, firstName } } = props;
  const brewer = team === 'MIL' ? 'list-group-item-success' : '';
  const cardsCubs = (team === 'STL' || team === 'CHC') ? 'list-group-item-danger' : '';
  return (
    <a 
      className={`list-group-item list-group-item-action col-6 ${brewer} ${cardsCubs}`}
      style={{ cursor: 'pointer' }}
      onClick={handleOnClick}
    >
      <p><strong>Last Name: </strong>{lastName} <strong>First Name: </strong>{firstName}</p>
    </a>      
  );
}

Player.propTypes = {
  player: PropTypes.shape({
    firstName: PropTypes.string,
    lastName: PropTypes.string,
    id: PropTypes.string,
    team: PropTypes.string
  }),
  onClick: PropTypes.func
}
