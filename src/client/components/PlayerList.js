import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Loader from 'react-loader-spinner'
import { Player } from './Player.js';

export const PlayerList = props => {
  const handleOnChange = event => {
    const { onChange } = props;
    const { target: { value } } = event;
    onChange(value);
  }

  const { success, playerList, onClick, loading } = props;
  if (!success) {
    return (
      <div className="row">
        <div className="col-sm-4">
          <p><strong>Unable to get players list, please try again later.</strong></p>
        </div>
      </div>
    );
  }

  if (loading) {
    // #B6922E is Brewer Gold, #0A2351 is Brewer Blue
    return (
      <Fragment>
        <Loader type="Circles" color="#0A2351" />
        <Loader type="Circles" color="#B6922E" />
      </Fragment>
    );
  }

  return (
    <div>
      <div className="row">
        <div className="col-sm-4">
          <label>
            Filter players by Last Name: 
            <input type="text" onChange={handleOnChange} />
          </label>
        </div>
      </div>
      <ul className="list-group row">
        {!loading && playerList.length === 0 && <p><strong>There are no players with that name, please change your search</strong></p>}
        {playerList.map(player => <Player player={player} onClick={onClick} key={player.id} />)}
      </ul>
    </div>
  );
}

PlayerList.propTypes = {
  loading: PropTypes.bool,
  playerList: PropTypes.arrayOf(PropTypes.shape({
    firstName: PropTypes.string,
    lastName: PropTypes.string,
    id: PropTypes.string,
    team: PropTypes.string
  })),
  success: PropTypes.bool,
  onChange: PropTypes.func.isRequired
}
