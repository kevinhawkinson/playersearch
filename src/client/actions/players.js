export const FETCH_PLAYERS_BEGIN   = 'FETCH_PLAYERS_BEGIN';
export const FETCH_PLAYERS_SUCCESS = 'FETCH_PLAYERS_SUCCESS';
export const FETCH_PLAYERS_FAILURE = 'FETCH_PLAYERS_FAILURE';
export const FILTER_PLAYER_SEARCH = 'FILTER_PLAYER_SEARCH';
export const SHOW_MODAL = 'SHOW_MODAL';
export const HIDE_MODAL = 'HIDE_MODAL';

export const fetchPlayersBegin = () => ({ type: FETCH_PLAYERS_BEGIN });

export const fetchPlayersSuccess = playerList => ({
  type: FETCH_PLAYERS_SUCCESS,
  payload: { playerList }
});

export const fetchPlayersFailure = error => ({
  type: FETCH_PLAYERS_FAILURE,
  payload: { error }
});

export const showModal = player => ({ type: SHOW_MODAL, payload: { player } });

export const hideModal = () => ({ type: HIDE_MODAL })

export const filterPlayers = search => ({
  type: FILTER_PLAYER_SEARCH,
  payload: { search }
})

export const fetchPlayers = () => {
  return dispatch => {
    dispatch(fetchPlayersBegin());
    return fetch('/getPlayerList')
      .then(handleErrors)
      .then(res => res.json())
      .then(json => {
        dispatch(fetchPlayersSuccess(json.playerList));
        return json.playerList;
      }).catch(error => dispatch(fetchPlayersFailure(error)));
  };
}
  
function handleErrors(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}