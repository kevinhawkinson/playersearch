import produce from "immer"
import {
    FETCH_PLAYERS_BEGIN,
    FETCH_PLAYERS_SUCCESS,
    FETCH_PLAYERS_FAILURE,
    FILTER_PLAYER_SEARCH,
    SHOW_MODAL,
    HIDE_MODAL
  } from '../actions/players.js';
  
const initialState = {
  completeList: [],
  filteredList: [],
  showModal: false,
  loading: false,
  error: null,
  player: {}
};
  
export default (state = initialState, action) => 
  produce(state, draft => {
    switch(action.type) {
      case FETCH_PLAYERS_BEGIN:
        draft.loading = true;
        return;

      case FETCH_PLAYERS_SUCCESS:
        draft.loading = false;
        draft.completeList = action.payload.playerList;
        draft.filteredList = action.payload.playerList;
        return;
  
      case FETCH_PLAYERS_FAILURE:
        draft.loading = false;
        draft.error = action.payload.error;
        return;
  
      case FILTER_PLAYER_SEARCH:
        draft.filteredList = draft.completeList.filter(player =>
          player.lastName.toLowerCase().includes(action.payload.search.toLowerCase())
        );
        return;
  
      case SHOW_MODAL:
        draft.showModal = true;
        draft.player = action.payload.player;
        return;
  
      case HIDE_MODAL:
        draft.showModal = false;
        return;
    }
  });