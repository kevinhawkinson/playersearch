import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux'
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import { PlayerSearch } from './containers/PlayerSearch.js';
import players from './reducers/players.js';

const logger = createLogger();
const store = createStore(players, applyMiddleware(logger, thunk));

ReactDOM.render(
  <Provider store={store}>
    <PlayerSearch />
  </Provider>,
  document.getElementById('root')
);
