import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { PlayerList } from '../components/PlayerList.js';
import { PlayerModal } from '../components/PlayerModal.js';
import * as playerActions from '../actions/players.js';

const PlayerSearchRaw = props => {
  useEffect(() => {
    const { fetchPlayers } = props;
    fetchPlayers();
  }, []);

  const handleOnChange = value => {
    const { filterPlayers } = props;
    filterPlayers(value);
  }

  const handleOnClick = player => {
    const { showModal } = props;
    showModal(player);
  }

  const handleModalClose = () => {
    const { hideModal } = props;
    hideModal();
  }

  const { filteredList, displayModal, player, error, loading } = props;
  let modal;
  if (displayModal) {
    modal = <PlayerModal player={player} onModalClose={handleModalClose} />
  }
  return (
    <div>
      {modal}
      <PlayerList
        loading={loading}
        playerList={filteredList}
        success={!error}
        onChange={handleOnChange}
        onClick={handleOnClick}
      />
    </div>
  );
}

PlayerSearchRaw.propTypes = {
  fetchPlayers: PropTypes.func.isRequired,
  filterPlayers: PropTypes.func.isRequired,
  showModal: PropTypes.func.isRequired,
  hideModal: PropTypes.func.isRequired,
  filteredList: PropTypes.arrayOf(PropTypes.shape({})),
  displayModal: PropTypes.bool,
  player: PropTypes.shape({}),
  error: PropTypes.shape({}),
  loading: PropTypes.bool
}

export const PlayerSearch = connect(
  state => ({
    filteredList: state.filteredList,
    displayModal: state.showModal,
    player: state.player,
    error: state.error,
    loading: state.loading
  }),
  dispatch => bindActionCreators({ ...playerActions }, dispatch)
)(PlayerSearchRaw);
