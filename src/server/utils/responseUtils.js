/*
  The response from CBS sports includes ALL players.  Including all minor league players
  and it also includes a 'player' for each MLB team.  That is bizarre.
*/
export function transformResponseBody(response, body) {
  const newBody = JSON.parse(body).body;
  const playerListSorted = newBody.players.reduce((ret, player) => {
    // Only display currently Active players on an MLB team
    if (player.pro_status === 'A') {
      return ret.concat({
        id: player.id,
        firstName: player.firstname,
        lastName: player.lastname,
        team: player.pro_team,
        position: player.position,
        photo: player.photo
      });
    }
    return ret;
  }, []).sort((playerA, playerB) => {
    // Sort the list by last name then by first name.
    const lastNameA = playerA.lastName.toUpperCase();
    const lastNameB = playerB.lastName.toUpperCase();
    if (lastNameA < lastNameB) {
      return -1;
    }
    if (lastNameA > lastNameB) {
      return 1;
    }
    const firstNameA = playerA.firstName.toUpperCase();
    const firstNameB = playerB.firstName.toUpperCase();
    if (firstNameA < firstNameB) {
      return -1;
    }
    if (firstNameA > firstNameB) {
      return 1;
    }
    return 0;
  });
  return {
    statusCode: response.statusCode,
    statusMessage: response.statusMessage,
    playerList: playerListSorted
  }
}
  