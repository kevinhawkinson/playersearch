import express from 'express';
import request from 'request';
import { transformResponseBody } from './utils/responseUtils.js';

const app = express();

const BASEBALL_PLAYER_API = {
  url: 'http://api.cbssports.com/fantasy/players/list?version=3.0&SPORT=baseball&response_format=JSON',
  method: 'GET'
};

app.use('/', express.static('public'));

app.get('/getPlayerList', (req, res) => {
  request.get(BASEBALL_PLAYER_API, (error, response, body) => {
    if (response.statusCode === 200) {
      const transformedResponse = transformResponseBody(response, body);
      res.json(transformedResponse);
    } else {
      res.json(response);
    }
  });
});

app.listen(process.env.PORT || 3001);
